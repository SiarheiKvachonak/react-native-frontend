import { StyleSheet, Platform } from 'react-native';
// constants
import COLORS from '../constants/colors';

export default StyleSheet.create({
  container: {
    padding: 24,
    flex: 1,
  },
  header: {
    backgroundColor: COLORS.lightBlue,
  },
  footer: {
    backgroundColor: COLORS.lightBlue,
    paddingTop: Platform.OS ? 10 : 0,
  },
  title: {
    fontFamily: 'nunito-bold',
    fontSize: 18,
  },
  titleCenter: {
    alignSelf: 'center',
  },
  screenHeader: {
    fontFamily: 'nunito-bold',
    fontSize: 18,
    alignSelf: 'center',
    marginBottom: 10,
  },
  loadingContainer: {
    flex: 1,
    justifyContent: 'center',
  },
  horizontal: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 10,
  },
  error: {
    color: 'red',
  },
  boldText: {
    fontFamily: 'nunito-bold',
  },
});

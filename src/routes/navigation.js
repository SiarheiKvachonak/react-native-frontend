// libraries
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { NavigationContainer, DrawerActions } from '@react-navigation/native';
import Animated from 'react-native-reanimated';
import { StyleSheet, AsyncStorage } from 'react-native';
import { createDrawerNavigator, DrawerContentScrollView, DrawerItemList, DrawerItem } from '@react-navigation/drawer';
import { AntDesign, Ionicons } from '@expo/vector-icons';
import { Container, Content, Header, Footer, Left, Thumbnail, Body, H3, Text, ListItem, Right } from 'native-base';
// components
import HomeStack from './homeStack';
import BasketStack from './basketStack';
import AuthStack from './authStack';
import BackgroundImage from '../shared/BackgroundImage';
// actions
import { signout } from '../actions';
// helpers
import { setNavigator } from '../helpers';
// constants
import { SCREENS, AUTH_TOKEN } from '../constants';
import COLORS from '../constants/colors';
// styles
import globalStyles from '../styles/globalStyles';

const defaultThumbnail =
  'https://yt3.ggpht.com/a/AATXAJxhcqf0aHOV0ceKloCTLiDXtCeMis1q30RBeQ=s900-c-k-c0xffffffff-no-rj-mo';

const AppTabs = createBottomTabNavigator();
const AppTabsScreen = ({ navigation }) => (
  <AppTabs.Navigator
    tabBarOptions={{
      activeTintColor: COLORS.darkBlue,
      style: globalStyles.footer,
    }}
  >
    <AppTabs.Screen
      name={SCREENS.home}
      component={HomeStack}
      options={{
        tabBarIcon: ({ size }) => (
          <AntDesign
            name="home"
            size={size}
            color={COLORS.darkBlue}
            onPress={() => navigation.navigate(SCREENS.home)}
          />
        ),
      }}
    />
    <AppTabs.Screen
      name={SCREENS.basket}
      component={BasketStack}
      options={{
        tabBarIcon: ({ size }) => (
          <Ionicons
            name="md-basket"
            size={size}
            color={COLORS.darkBlue}
            onPress={() => navigation.navigate(SCREENS.basket)}
          />
        ),
      }}
    />
  </AppTabs.Navigator>
);

const Sidebar = ({ progress, ...props }) => {
  const dispatch = useDispatch();
  const { token } = useSelector(({ auth }) => auth);
  const { navigation } = props;

  const translateX = Animated.interpolate(progress, {
    inputRange: [0, 1],
    outputRange: [-100, 0],
  });

  return (
    <Container>
      <Header style={styles.header}>
        <Right>
          <Ionicons
            name="ios-arrow-dropright"
            size={24}
            color="black"
            onPress={() => navigation.dispatch(DrawerActions.closeDrawer())}
          />
        </Right>
      </Header>
      <Content>
        <BackgroundImage />
        {token && (
          <ListItem thumbnail>
            <Left>
              <Thumbnail
                source={{
                  uri: defaultThumbnail,
                }}
              />
            </Left>
            <Body>
              <H3>John</H3>
              <Text note>Customer</Text>
            </Body>
          </ListItem>
        )}
        <DrawerContentScrollView {...props}>
          <Animated.View
            style={{
              transform: [{ translateX }],
            }}
          >
            <DrawerItemList {...props} />
          </Animated.View>
        </DrawerContentScrollView>
      </Content>
      <Footer style={styles.footer}>
        {token && (
          <DrawerItem
            label="Sign out"
            icon={({ size }) => <AntDesign name="logout" size={size} color="black" />}
            onPress={async () => {
              dispatch(signout());
              await AsyncStorage.removeItem(AUTH_TOKEN);
            }}
          />
        )}
      </Footer>
    </Container>
  );
};

const AppDrawer = createDrawerNavigator();

const AppDrawerScreen = () => (
  <AppDrawer.Navigator
    drawerContent={(props) => <Sidebar {...props} />}
    drawerType="slide"
    drawerStyle={{
      fontSize: 16,
      backgroundColor: COLORS.lightBlue,
      width: 240,
    }}
    drawerContentOptions={{
      activeTintColor: COLORS.darkBlue2,
      itemStyle: { marginVertical: 10 },
    }}
  >
    <AppDrawer.Screen
      name={SCREENS.tabs}
      component={AppTabsScreen}
      options={{
        drawerLabel: 'Home',
        drawerIcon: ({ size }) => <AntDesign name="home" size={size} color={COLORS.darkBlue} />,
      }}
    />
  </AppDrawer.Navigator>
);

export default () => {
  const { token } = useSelector(({ auth }) => auth);

  return (
    <NavigationContainer
      ref={(navigation) => {
        setNavigator(navigation);
      }}
    >
      {token ? <AppDrawerScreen /> : <AuthStack />}
    </NavigationContainer>
  );
};

const styles = StyleSheet.create({
  header: {
    backgroundColor: COLORS.lightBlue1,
  },
  footer: {
    backgroundColor: COLORS.lightBlue1,
  },
  wrapper: {
    ...StyleSheet.absoluteFill,
  },
});

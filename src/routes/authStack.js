// libraries
import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
// components
import Signin from '../screens/Auth/SignIn';
// constants
import { SCREENS } from '../constants';

const Stack = createStackNavigator();

const AuthStack = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen name={SCREENS.signin} component={Signin} options={{ headerShown: false }} />
    </Stack.Navigator>
  );
};

export default AuthStack;

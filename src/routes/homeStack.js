// libraries
import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
// components
import Header from '../shared/header';
import Home from '../screens/Home';
import Details from '../screens/details';
// constants
import { SCREENS } from '../constants';
import COLORS from '../constants/colors';
// styles
import globalStyles from '../styles/globalStyles';

const Stack = createStackNavigator();

const Navigator = ({ navigation }) => (
  <Stack.Navigator>
    <Stack.Screen
      name={SCREENS.home}
      component={Home}
      options={() => ({
        headerTitle: () => <Header navigation={navigation} title={SCREENS.home} />,
        headerStyle: globalStyles.header,
      })}
    />
    <Stack.Screen
      name={SCREENS.details}
      component={Details}
      options={() => ({
        headerTitle: () => <Header navigation={navigation} title={SCREENS.details} />,
        headerStyle: globalStyles.header,
        headerTintColor: COLORS.darkBlue2,
      })}
    />
  </Stack.Navigator>
);

export default Navigator;

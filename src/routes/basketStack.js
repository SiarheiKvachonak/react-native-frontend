// libraries
import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
// components
import Basket from '../screens/Basket';
import Header from '../shared/header';
import Payment from '../screens/Payment';
// constants
import { SCREENS } from '../constants';
// styles
import globalStyles from '../styles/globalStyles';
import COLORS from '../constants/colors';

const Stack = createStackNavigator();

const BasketStack = ({ navigation }) => (
  <Stack.Navigator>
    <Stack.Screen
      name={SCREENS.basket}
      component={Basket}
      options={() => ({
        headerTitle: () => <Header navigation={navigation} title={SCREENS.basket} />,
        headerStyle: globalStyles.header,
      })}
    />
    <Stack.Screen
      name={SCREENS.payment}
      component={Payment}
      options={() => ({
        headerTitle: () => <Header navigation={navigation} title={SCREENS.payment} />,
        headerStyle: globalStyles.header,
        headerTintColor: COLORS.darkBlue2,
      })}
    />
  </Stack.Navigator>
);

export default BasketStack;

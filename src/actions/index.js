import {
  ADD_INTO_BASKET,
  REMOVE_FROM_BASKET,
  CLEAR_BASKET,
  SIGNUP,
  SIGNIN,
  SIGNOUT,
  GET_REMOTE_BASKET,
} from '../constants/actionTypes';

export const getRemoteBasket = (item) => ({
  type: GET_REMOTE_BASKET,
  item,
});

export const addToBasket = (item) => ({
  type: ADD_INTO_BASKET,
  item,
});

export const removeFromBasket = (item) => ({
  type: REMOVE_FROM_BASKET,
  item,
});

export const clearBasket = () => ({
  type: CLEAR_BASKET,
});

export const signup = (item) => ({
  type: SIGNUP,
  item,
});

export const signin = (item) => ({
  type: SIGNIN,
  item,
});

export const signout = () => ({
  type: SIGNOUT,
});

// libraries
import { CommonActions } from '@react-navigation/native';

let navigation;

export const setNavigator = (nav) => {
  navigation = nav;
};

export const navigate = (name, params) => {
  if (!navigation) return;

  navigation.dispatch(
    CommonActions.navigate({
      name,
      params,
    }),
  );
};

export const addItemToBasket = (currentBasket, item) => {
  const currentCount = currentBasket?.count;
  const total = currentBasket?.total + item.price;
  let currentItems = [...currentBasket.items];
  const index = currentItems.findIndex(({ _id }) => _id === item?._id);

  if (index < 0) {
    const currentItem = {
      ...item,
      total: 1,
      totalPrice: item.price,
    };
    currentItems = [...currentItems, currentItem];
  } else {
    const totalItems = currentItems[index].total + 1;
    const currentItem = {
      ...item,
      total: totalItems,
      totalPrice: totalItems * item.price,
    };
    currentItems[index] = currentItem;
  }

  return {
    ...currentBasket,
    items: currentItems,
    total,
    count: currentCount + 1,
  };
};

export const removeItemFromBasket = (currentBasket, item) => {
  let currentCount = currentBasket?.count;
  const currentItems = [...currentBasket.items];

  const index = currentItems.findIndex(({ _id }) => _id === item?._id);
  if (index < 0) return currentBasket;
  currentCount -= 1;
  const total = currentBasket?.total - item.price;

  currentItems[index].total = currentItems[index].total - 1;
  currentItems[index].totalPrice = currentItems[index].total * item.price;

  const filtered = currentItems.filter(({ total: itemsCount }) => itemsCount > 0);

  return {
    ...currentBasket,
    items: filtered,
    total,
    count: currentCount,
  };
};

export const clearWholeBasket = () => {
  return {
    items: [],
    total: 0,
    count: 0,
  };
};

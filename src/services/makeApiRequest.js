// libraries
import axios from 'axios';
// config
import store from '../reducers/store';

const base = 'https://fathomless-shelf-48717.herokuapp.com';

const makeRequest = () => {
  const state = store.getState();
  const {
    auth: { token },
  } = state;

  const API = axios;
  API.interceptors.request.use((config) => {
    config.headers.Authorization = `Bearer ${token}`;
    config.baseURL = base;
    config.responseType = 'json';

    return config;
  });

  return API;
};

export default makeRequest;

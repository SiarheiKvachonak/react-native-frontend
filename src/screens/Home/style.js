export default {
  info: {
    padding: 10,
  },
  header: {
    fontSize: 20,
    fontFamily: 'nunito-bold',
    marginBottom: 15,
  },
  cardImage: {
    height: 100,
    width: '100%',
  },
  price: {
    fontSize: 22,
    fontFamily: 'nunito-bold',
  },
};

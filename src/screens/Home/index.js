// libraries
import React, { useState, useCallback, useEffect } from 'react';
// components
import { View, Text, FlatList, TouchableOpacity, Image, ActivityIndicator } from 'react-native';
import Card from '../../shared/Card';
// services
import makeRequest from '../../services/makeApiRequest';
// constants
import { SCREENS } from '../../constants';
import COLORS from '../../constants/colors';
// styles
import globalStyles from '../../styles/globalStyles';
import styles from './style';

const Home = ({ navigation }) => {
  const [products, setProducts] = useState([]);
  const [isLoading, setLoading] = useState(false);
  const [error, setError] = useState(null);

  const getProducts = useCallback(async () => {
    setLoading(true);
    try {
      const result = await makeRequest().get('/products');
      setProducts(result?.data);
    } catch (apiError) {
      setError(apiError?.response?.data?.error);
    } finally {
      setLoading(false);
    }
  }, []);

  useEffect(() => {
    navigation.addListener('focus', () => {
      getProducts();
    });
  }, [getProducts]);

  useEffect(
    () =>
      navigation.addListener('blur', () => {
        setError(null);
      }),
    [],
  );

  if (isLoading) {
    return (
      <View style={[globalStyles.loadingContainer, globalStyles.horizontal]}>
        <ActivityIndicator size="large" color={COLORS.darkBlue} />
      </View>
    );
  }

  return (
    <View style={globalStyles.container}>
      <Text style={globalStyles.screenHeader}>Home screen</Text>
      {error && <Text>{error}</Text>}

      <FlatList
        data={products}
        keyExtractor={(item) => item._id.toString()}
        renderItem={({ item }) => {
          const { title, image, description, price } = item || {};
          return (
            <TouchableOpacity
              onPress={() => {
                navigation.navigate(SCREENS.details, item);
              }}
            >
              <Card>
                <Image style={styles.cardImage} source={{ uri: image }} />
                <View style={styles.info}>
                  <Text style={styles.header}>{title}</Text>
                  <Text>{description}</Text>
                  <Text style={styles.price}>${price}</Text>
                </View>
              </Card>
            </TouchableOpacity>
          );
        }}
      />
    </View>
  );
};

export default Home;

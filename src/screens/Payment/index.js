// libraries
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { CreditCardInput } from 'react-native-credit-card-input';
import { View, Text, ActivityIndicator } from 'react-native';
// components
import Button from '../../shared/Button';
import BackgroundImage from '../../shared/BackgroundImage';
// actions
import { clearBasket } from '../../actions';
// services
import makeRequest from '../../services/makeApiRequest';
import { clearWholeBasket } from '../../helpers';
import config from '../../../config';
// constants
import { BASE_CURRENSY, SCREENS } from '../../constants';
import COLORS from '../../constants/colors';
// styles
import globalStyles from '../../styles/globalStyles';
import styles from './styles';

const stripe = require('stripe-client')(config.REACT_STRIPE_KEY);

const Payment = ({ navigation }) => {
  const dispatch = useDispatch();
  const { total: billTotal } = useSelector(({ basket }) => basket || {});
  const [cardData, setCardData] = useState({});
  const [paySuccess, setSuccess] = useState(false);
  const [isLoading, setSLoading] = useState(false);
  const [error, setError] = useState(false);

  useEffect(
    () =>
      navigation.addListener('focus', () => {
        if (!billTotal) {
          navigation.navigate(SCREENS.basket);
        }
      }),
    [billTotal],
  );

  const pay = async () => {
    try {
      const {
        values: { cvc, expiry, number },
        valid: isCardValid,
      } = cardData;
      const expirations = expiry.split('/') || [];

      if (!isCardValid) return;

      const paymentDetails = {
        card: {
          number,
          exp_month: expirations[0],
          exp_year: expirations[1],
          cvc,
        },
      };

      const stripeResponse = await stripe.createToken(paymentDetails);
      const tokenId = stripeResponse?.id;

      const paymentData = {
        amount: Math.round(billTotal * 100),
        currency: BASE_CURRENSY,
        payment_method_data: {
          type: 'card',
          'card[token]': tokenId,
        },
      };

      setSLoading(true);
      await makeRequest().post('/pay', paymentData);
      setSuccess(true);
      dispatch(clearBasket());
      await makeRequest().patch('/basket', clearWholeBasket());
    } catch (err) {
      setError(err?.response?.data?.error);
    } finally {
      setSLoading(false);
    }
  };

  if (isLoading && !paySuccess) {
    return (
      <>
        <View style={[globalStyles.loadingContainer, globalStyles.horizontal]}>
          <ActivityIndicator size="large" color={COLORS.darkBlue} />
        </View>
        <Text style={styles.processingText}>Still processing...</Text>
      </>
    );
  }

  return (
    <View style={globalStyles.container}>
      <BackgroundImage />
      <Text style={globalStyles.screenHeader}>Your order</Text>
      {error && <Text style={globalStyles.error}>{error}</Text>}

      <View style={styles.cardWrapper}>
        <CreditCardInput onChange={(form) => setCardData(form)} />
      </View>

      {paySuccess && (
        <View style={styles.completeContainer}>
          <Text style={styles.completeText}>Your order was successfully paid. </Text>
          <Text style={styles.completeText}>Thank you!</Text>
        </View>
      )}
      {!paySuccess && (
        <>
          <View style={[styles.container, styles.totalPrice]}>
            <Text style={styles.price}>Total</Text>
            <Text style={styles.price}>${billTotal}</Text>
          </View>

          <View style={styles.button}>
            <Button title="Pay" pressHandler={pay} />
          </View>
        </>
      )}
    </View>
  );
};

export default Payment;

// libraries
import { StyleSheet } from 'react-native';
// constants
import COLORS from '../../constants/colors';

export default StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  price: {
    fontFamily: 'nunito-bold',
    fontSize: 22,
    textAlignVertical: 'center',
  },
  button: {
    marginVertical: 40,
  },
  completeContainer: {
    paddingTop: 40,
    alignItems: 'center',
  },
  completeText: {
    color: COLORS.lightGreen,
    fontFamily: 'nunito-bold',
    fontSize: 16,
  },
  itemName: {
    fontSize: 16,
    textAlignVertical: 'center',
  },
  processingText: {
    alignSelf: 'center',
    bottom: 250,
    color: COLORS.darkBlue,
    fontSize: 16,
    position: 'absolute',
  },
  cardWrapper: {
    marginTop: 20,
    marginBottom: 60,
  },
});

// libraries
import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import * as Animatable from 'react-native-animatable';
import { View, Text, TouchableOpacity, TextInput, Platform, StyleSheet, AsyncStorage } from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Feather from 'react-native-vector-icons/Feather';
// components
import Button from '../../shared/Button';
import BackgroundImage from '../../shared/BackgroundImage';
// actions
import { signin } from '../../actions';
// services
import makeRequest from '../../services/makeApiRequest';
// constants
import { AUTH_TOKEN, EMAIL_VALIDATION } from '../../constants';
import COLORS from '../../constants/colors';

const requests = {
  signin: 'signin',
  signup: 'signup',
};

const Signin = ({ navigation }) => {
  const dispatch = useDispatch();

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [error, setError] = useState(null);
  const [options, setOptions] = useState({
    secureTextEntry: true,
    isEmailValid: false,
    isUserProvideEmail: false,
  });

  const updateSecureTextEntry = () => {
    setOptions({
      ...options,
      secureTextEntry: !options.secureTextEntry,
    });
  };

  const checkEmail = () => {
    setOptions({
      ...options,
      isEmailValid: EMAIL_VALIDATION.test(email.trim()),
      isUserProvideEmail: true,
    });
  };

  useEffect(
    () =>
      navigation.addListener('blur', () => {
        setError(null);
      }),
    [],
  );

  const submit = async (requestName) => {
    setError(null);
    if (!options.isEmailValid) {
      setError('Please fill all fields!');
      return;
    }

    try {
      const requestBody = {
        email: email.trim(),
        password: password.trim(),
      };

      const response = await makeRequest().post(`/${requestName}`, requestBody);
      const { token } = response?.data;
      await AsyncStorage.setItem(AUTH_TOKEN, token);
      dispatch(signin({ token }));
    } catch (err) {
      setError(err?.response?.data?.error);
    }
  };

  return (
    <View style={styles.container}>
      <BackgroundImage />
      <View style={styles.header}>
        <Text style={styles.textHeader}>Welcome!</Text>
      </View>
      <Animatable.View
        animation="fadeInUpBig"
        style={[
          styles.footer,
          {
            backgroundColor: 'transparent',
          },
        ]}
      >
        {error && <Text style={styles.errorMsg}>{error}</Text>}
        <Text
          style={[
            styles.text_footer,
            {
              marginTop: 35,
            },
          ]}
        >
          Email
        </Text>
        <View style={styles.action}>
          <FontAwesome name="user-o" size={20} />
          <TextInput
            name="email"
            placeholder="Your email"
            placeholderTextColor={COLORS.grey2}
            style={styles.textInput}
            autoCapitalize="none"
            onChangeText={setEmail}
            onEndEditing={checkEmail}
            onFocus={() => {
              setError(null);
            }}
          />
          {options.isEmailValid && (
            <Animatable.View animation="bounceIn">
              <Feather name="check-circle" color="green" size={20} />
            </Animatable.View>
          )}
        </View>
        {!options.isEmailValid && options.isUserProvideEmail && (
          <Animatable.View animation="fadeInLeft" duration={500}>
            <Text style={styles.errorMsg}>Provide correct email</Text>
          </Animatable.View>
        )}

        <Text
          style={[
            styles.text_footer,
            {
              marginTop: 35,
            },
          ]}
        >
          Password
        </Text>
        <View style={styles.action}>
          <Feather name="lock" size={20} />
          <TextInput
            name="password"
            placeholder="Your Password"
            placeholderTextColor="#666666"
            secureTextEntry={options.secureTextEntry}
            style={styles.textInput}
            autoCapitalize="none"
            onChangeText={setPassword}
            onFocus={() => {
              setError(null);
            }}
          />
          <TouchableOpacity onPress={updateSecureTextEntry}>
            {options.secureTextEntry ? (
              <Feather name="eye-off" color="grey" size={20} />
            ) : (
              <Feather name="eye" color="grey" size={20} />
            )}
          </TouchableOpacity>
        </View>

        <View style={styles.button}>
          <Button
            title="Sign In"
            pressHandler={async () => {
              await submit(requests.signin);
            }}
          />
        </View>

        <View style={styles.button}>
          <Button
            title="Sign Up"
            pressHandler={async () => {
              await submit(requests.signup);
            }}
          />
        </View>
      </Animatable.View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    flex: 1,
    justifyContent: 'flex-end',
    paddingHorizontal: 20,
    paddingBottom: 30,
  },
  footer: {
    flex: 3,
    backgroundColor: '#fff',
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    paddingHorizontal: 20,
    paddingVertical: 30,
  },
  textHeader: {
    color: COLORS.lightBlue1,
    fontWeight: 'bold',
    fontSize: 30,
    alignSelf: 'center',
  },
  text_footer: {
    color: '#05375a',
    fontSize: 18,
  },
  action: {
    flexDirection: 'row',
    marginTop: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#f2f2f2',
    paddingBottom: 5,
  },
  actionError: {
    flexDirection: 'row',
    marginTop: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#FF0000',
    paddingBottom: 5,
  },
  textInput: {
    fontSize: 18,
    fontFamily: 'nunito-regular',
    flex: 1,
    marginTop: Platform.OS === 'ios' ? 0 : -12,
    paddingLeft: 10,
    color: '#05375a',
  },
  errorMsg: {
    color: '#FF0000',
    fontSize: 14,
  },
  button: {
    alignItems: 'center',
    marginTop: 30,
  },
});

export default Signin;

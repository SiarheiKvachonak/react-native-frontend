// libraries
import React, { useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';
// components
import { View, Text, Alert, StyleSheet } from 'react-native';
import Button from '../shared/Button';
// actions
import { addToBasket } from '../actions';
// helpers
import { addItemToBasket } from '../helpers';
// services
import makeRequest from '../services/makeApiRequest';
// styles
import globalStyles from '../styles/globalStyles';

const Details = ({ route: { params = {} } }) => {
  const dispatch = useDispatch();
  const { basket } = useSelector((store) => store);
  const { items = [] } = basket;

  const isInBasket = useMemo(() => !!items.find(({ _id }) => _id === params?._id), [items, params?._id]);

  const addItem = async () => {
    const newBasket = addItemToBasket(basket, params);
    dispatch(addToBasket(newBasket));
    try {
      await makeRequest().patch('/basket', newBasket);
    } catch (err) {
      console.log(err?.response?.data?.error);
    }

    return Alert.alert('Success', `${params.title} was added into basket`, [
      {
        text: 'Close',
        onPress: () => {},
      },
    ]);
  };

  return (
    <View style={globalStyles.container}>
      <Text style={globalStyles.screenHeader}>Full Description</Text>
      <Text style={styles.description}>{params?.description}</Text>
      {!isInBasket && (
        <View style={styles.button}>
          <Button title="Add to basket" pressHandler={addItem} />
        </View>
      )}
      {isInBasket && <Text style={styles.description}>Item has already been added into basket.</Text>}
    </View>
  );
};

const styles = StyleSheet.create({
  description: {
    fontSize: 16,
    marginVertical: 20,
    alignSelf: 'center',
  },
  button: {
    position: 'absolute',
    bottom: 40,
    width: '100%',
    alignSelf: 'center',
  },
});

export default Details;

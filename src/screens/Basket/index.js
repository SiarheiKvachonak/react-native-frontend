// libraries
import React from 'react';
import { useSelector } from 'react-redux';
// components
import { View, FlatList, Image, Text } from 'react-native';
import Card from '../../shared/Card';
import Counter from '../../shared/Counter';
import Button from '../../shared/Button';
// constants
import MESSAGES from '../../constants/messages';
import { SCREENS } from '../../constants';
// styles
import globalStyles from '../../styles/globalStyles';
import styles from './styles';

const Basket = ({ navigation }) => {
  const { items = [], count, total } = useSelector(({ basket }) => basket || {});
  const isBasketEmpty = !items.length;

  return (
    <View style={globalStyles.container}>
      <Text style={globalStyles.screenHeader}>Items</Text>
      <View style={styles.infoWrapper}>
        <Text style={globalStyles.boldText}> Total: ${total}</Text>
        <Text style={globalStyles.boldText}>{`Items ${count} in basket`}</Text>
      </View>
      {isBasketEmpty && <Text style={styles.emptyText}>{MESSAGES.emptyBasket}</Text>}
      {!isBasketEmpty && (
        <View style={styles.itemContainer}>
          <FlatList
            data={items}
            keyExtractor={(item) => item._id.toString()}
            renderItem={({ item }) => {
              const { title, image, totalPrice } = item || {};

              return (
                <Card>
                  <Text style={styles.header}>{title}</Text>
                  <View style={styles.container}>
                    <View>
                      <Image style={styles.cardImage} source={{ uri: image }} />
                    </View>

                    <Text style={styles.price}>${totalPrice}</Text>
                  </View>
                  <View style={styles.counterWrapper}>
                    <Counter item={item} />
                  </View>
                </Card>
              );
            }}
          />
        </View>
      )}
      {!isBasketEmpty && (
        <View style={styles.button}>
          <Button title="Proceed to Payment" pressHandler={() => navigation.navigate(SCREENS.payment)} />
        </View>
      )}
    </View>
  );
};

export default Basket;

export default {
  container: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  infoWrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginVertical: 10,
  },
  itemContainer: {
    paddingTop: 10,
    height: 500,
    marginBottom: 20,
  },
  cardImage: {
    height: 50,
    width: 50,
    resizeMode: 'contain',
  },
  price: {
    fontFamily: 'nunito-bold',
    fontSize: 22,
    textAlignVertical: 'center',
  },
  header: {
    fontFamily: 'nunito-bold',
    fontSize: 16,
    alignSelf: 'center',
    marginBottom: 15,
  },
  counterWrapper: {
    marginTop: 10,
    alignItems: 'center',
  },
  button: {
    position: 'absolute',
    bottom: 40,
    width: '100%',
    alignSelf: 'center',
  },
  emptyText: {
    alignSelf: 'center',
    fontFamily: 'nunito-bold',
    fontSize: 16,
    marginVertical: 20,
  },
};

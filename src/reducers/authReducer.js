// constants
import { SIGNUP, SIGNIN, SIGNOUT } from '../constants/actionTypes';

const initialState = {
  token: null,
  isAdmin: false,
};

const authReducer = (state = initialState, action) => {
  if (action.type === SIGNUP) {
    const { token } = action.item;

    return {
      ...state,
      token,
    };
  }

  if (action.type === SIGNIN) {
    const { token, isAdmin = false } = action.item;

    return {
      ...state,
      token,
      isAdmin,
    };
  }

  if (action.type === SIGNOUT) {
    return {
      ...state,
      token: null,
      isAdmin: null,
    };
  }

  return state;
};

export default authReducer;

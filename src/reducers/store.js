import { createStore } from 'redux';
import rootReducer from './rootReducer';

const configureStore = () => createStore(rootReducer);
const store = configureStore();

export default store;

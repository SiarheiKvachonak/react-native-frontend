import { ADD_INTO_BASKET, REMOVE_FROM_BASKET, CLEAR_BASKET, GET_REMOTE_BASKET } from '../constants/actionTypes';

const initialState = {
  items: [],
  total: 0,
  count: 0,
};

const basketReducer = (state = initialState, action) => {
  const { item = {} } = action || {};

  if (action.type === GET_REMOTE_BASKET) {
    const { total, count, basket = [] } = item;
    const items = Array.from(basket, ({ productId, count: itemsCount = 0, totalPrice = 0 }) => ({
      ...productId,
      total: itemsCount,
      totalPrice,
    }));

    return {
      ...state,
      items,
      total: total || 0,
      count: count || 0,
    };
  }

  if (action.type === ADD_INTO_BASKET) {
    return {
      ...state,
      ...item,
    };
  }

  if (action.type === REMOVE_FROM_BASKET) {
    return {
      ...state,
      ...item,
    };
  }

  if (action.type === CLEAR_BASKET) {
    return {
      ...state,
      items: [],
      total: 0,
      count: 0,
    };
  }

  return state;
};

export default basketReducer;

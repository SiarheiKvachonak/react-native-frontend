import { combineReducers } from 'redux';
import basketReducer from './basketReducer';
import authReducer from './authReducer';

export default combineReducers({
  basket: basketReducer,
  auth: authReducer,
});

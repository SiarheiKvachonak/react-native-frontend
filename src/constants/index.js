export const SCREENS = {
  details: 'Details',
  home: 'Home',
  about: 'About',
  basket: 'Basket',
  payment: 'Payment',
  signin: 'Signin',
  signup: 'Signup',
  tabs: 'Tabs',
  auth: 'Authorization',
};

export const AUTH_TOKEN = 'AUTH_TOKEN';

export const BASE_CURRENSY = 'usd';

export const EMAIL_VALIDATION = /^(?=.{3,255}$)([a-zA-Z0-9._\-+])+@([a-zA-Z0-9.\-+])+\.[a-zA-Z]{1,}$/;

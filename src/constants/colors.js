export default {
  white: '#fff',
  grey: '#eee',
  grey1: '#333',
  grey2: '#666',
  pink: '#e91e63',
  darkBlue: '#034f84',
  darkBlue2: '#3c7cb4',
  lightBlue: '#caede9',
  lightBlue1: '#8abdb9',
  red: '#ff0000',
};

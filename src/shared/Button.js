// libraries
import React from 'react';
// components
import { StyleSheet, Text, TouchableOpacity } from 'react-native';
// constants
import COLORS from '../constants/colors';

export default ({ title, pressHandler }) => {
  return (
    <TouchableOpacity style={styles.button} onPress={pressHandler}>
      <Text
        style={[
          styles.title,
          {
            color: COLORS.white,
          },
        ]}
      >
        {title}
      </Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  button: {
    width: '100%',
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
    backgroundColor: COLORS.lightBlue1,
  },
  title: {
    fontSize: 18,
    fontWeight: 'bold',
  },
});

// libraries
import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    flexDirection: 'row',
    padding: 5,
  },
  input: {
    fontFamily: 'nunito-bold',
    fontSize: 22,
    marginHorizontal: 15,
    textAlignVertical: 'center',
  },
});

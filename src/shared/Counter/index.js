// libraries
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
// components
import { View, Text } from 'react-native';
import { Entypo } from '@expo/vector-icons';
// actions
import { addToBasket, removeFromBasket } from '../../actions';
// helpers
import { addItemToBasket, removeItemFromBasket } from '../../helpers';
// constants
import COLORS from '../../constants/colors';
// styles
import styles from './styles';
import makeRequest from '../../services/makeApiRequest';

const Counter = ({ item }) => {
  const dispatch = useDispatch();
  const { basket } = useSelector((store) => store);
  const { total } = item || {};

  const addItem = async () => {
    const newBasket = addItemToBasket(basket, item);
    dispatch(addToBasket(newBasket));
    await makeRequest().patch('/basket', newBasket);
  };

  const removeItem = async () => {
    const newBasket = removeItemFromBasket(basket, item);
    dispatch(removeFromBasket(newBasket));
    await makeRequest().patch('/basket', newBasket);
  };

  return (
    <View style={styles.container}>
      <Entypo name="squared-minus" onPress={removeItem} size={40} color={COLORS.darkBlue} />
      <Text style={styles.input}>{total}</Text>
      <Entypo name="squared-plus" onPress={addItem} size={40} color={COLORS.darkBlue} />
    </View>
  );
};

export default Counter;

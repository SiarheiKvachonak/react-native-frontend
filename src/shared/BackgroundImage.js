import React from 'react';
import { Image, StyleSheet, View } from 'react-native';

const BackgroundImage = () => (
  <View style={styles.wrapper}>
    <Image source={require('../../assets/background.jpg')} />
  </View>
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  wrapper: {
    ...StyleSheet.absoluteFill,
    top: -20,
  },
});

export default BackgroundImage;

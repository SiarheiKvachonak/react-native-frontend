// components
import { StyleSheet, Platform } from 'react-native';
// constants
import COLORS from '../../constants/colors';

export default StyleSheet.create({
  header: {
    width: '100%',
    height: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  headerText: {
    fontWeight: 'bold',
    fontSize: 20,
    color: COLORS.grey1,
    letterSpacing: 1,
  },
  icon: {
    color: COLORS.darkBlue,
    position: 'absolute',
    ...Platform.select({
      ios: {
        left: -30,
      },
      android: {
        left: 16,
      },
    }),
  },
});

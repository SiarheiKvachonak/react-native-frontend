// libraries
import React from 'react';
import { useRoute } from '@react-navigation/native';
// components
import { Text, View } from 'react-native';
import { MaterialIcons } from '@expo/vector-icons';
import Basket from '../Basket';
// constants
import { SCREENS } from '../../constants';
// styles
import styles from './styles';

const Header = ({ title, navigation }) => {
  const { name } = useRoute();
  const isBasketScreen = name === SCREENS.basket;
  const openMenu = () => {
    navigation.openDrawer();
  };

  return (
    <View style={styles.header}>
      <MaterialIcons name="menu" size={28} onPress={openMenu} style={styles.icon} />
      <View>
        <Text style={styles.headerText}>{title}</Text>
      </View>

      {!isBasketScreen && <Basket />}
    </View>
  );
};

export default Header;

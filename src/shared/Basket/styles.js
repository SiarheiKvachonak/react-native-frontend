// components
import { Platform, StyleSheet } from 'react-native';
// constants
import COLORS from '../../constants/colors';

export default StyleSheet.create({
  basketWrapper: {
    flexDirection: 'row',
    position: 'absolute',

    ...Platform.select({
      ios: {
        right: -50,
      },
      android: {
        right: 16,
      },
    }),
  },
  basketIcon: {
    color: COLORS.darkBlue,
    fontSize: 28,
    marginHorizontal: 5,
  },
  counter: {
    color: COLORS.darkBlue,
    fontSize: 14,
    fontFamily: 'nunito-bold',
  },
});

// libraries
import React from 'react';
import { useSelector } from 'react-redux';
// components
import { Text, TouchableOpacity, Alert } from 'react-native';
import { MaterialIcons } from '@expo/vector-icons';
// helpers
import { navigate } from '../../helpers';
// constants
import { SCREENS } from '../../constants';
// styles
import styles from './styles';

const Basket = () => {
  const { count } = useSelector(({ basket }) => basket);

  const fullBasket = {
    text: 'Go to basket',
    onPress: () => navigate(SCREENS.basket),
  };
  let buttons = [{ text: 'Back', onPress: () => {} }];
  buttons = count ? [...buttons, fullBasket] : buttons;

  let basketMessage = `You have ${count} items in the basket.`;

  basketMessage = count ? `${basketMessage}  Would you like to proceed to payment?` : basketMessage;

  return (
    <TouchableOpacity onPress={() => Alert.alert('Basket', `${basketMessage}`, buttons)} style={styles.basketWrapper}>
      <MaterialIcons name="shopping-basket" style={styles.basketIcon} />
      {!!count && <Text style={styles.counter}>{count}</Text>}
    </TouchableOpacity>
  );
};

export default Basket;

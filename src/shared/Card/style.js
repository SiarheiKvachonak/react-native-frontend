import { StyleSheet } from 'react-native';
import COLORS from '../../constants/colors';

export default StyleSheet.create({
  card: {
    borderRadius: 6,
    elevation: 3,
    backgroundColor: COLORS.white,
    shadowOffset: { width: 1, height: 1 },
    shadowColor: COLORS.grey1,
    shadowOpacity: 0.3,
    shadowRadius: 2,
    marginHorizontal: 4,
    marginVertical: 6,
  },
  cardContent: {
    marginHorizontal: 18,
    marginVertical: 20,
  },
});

// libraries
import React from 'react';
// components
import { View } from 'react-native';
// styles
import styles from './style';

const Card = ({ children }) => (
  <View style={styles.card}>
    <View style={styles.cardContent}>{children}</View>
  </View>
);

export default Card;

import { REACT_STRIPE_KEY } from 'react-native-dotenv';

export default { REACT_STRIPE_KEY };

// libraries
import React, { useCallback, useEffect, useState } from 'react';
import { Provider } from 'react-redux';
import { Asset } from 'expo-asset';
import * as Font from 'expo-font';
import { AppLoading } from 'expo';
import { AsyncStorage, View, ActivityIndicator, Image } from 'react-native';
// components
import Navigation from './src/routes/navigation';
// actions
import { signin, getRemoteBasket } from './src/actions';
// services
import makeRequest from './src/services/makeApiRequest';
// config
import store from './src/reducers/store';
// constants
import { AUTH_TOKEN } from './src/constants';

const getFonts = () =>
  Font.loadAsync({
    'nunito-regular': require('./assets/fonts/Nunito-Regular.ttf'),
    'nunito-bold': require('./assets/fonts/Nunito-Bold.ttf'),
  });

function cacheImages(images) {
  return images.map(image => {
    if (typeof image === 'string') {
      return Image.prefetch(image);
    } else {
      return Asset.fromModule(image).downloadAsync();
    }
  });
}

const loadAssetsAsync = async () => {
  const imageAssets = cacheImages([require('./assets/background.jpg')]);

  const fontAssets = cacheFonts([getFonts()]);

  await Promise.all([...imageAssets, ...fontAssets]);
};

const getBasket = async () => {
  try {
    const result = await makeRequest().get('/basket');
    return result?.data;
  } catch (err) {
    console.log("Basket wasn't uploaded due to error.");
  }
};

export default function App() {
  const [fontsLoaded, setLoaded] = useState(false);
  const [isTokenLoaded, setTokenLoaded] = useState(false);

  const getStorageToken = useCallback(async () => {
    try {
      const token = await AsyncStorage.getItem(AUTH_TOKEN);
      store.dispatch(signin({ token }));
      const remoteBasket = await getBasket();
      store.dispatch(getRemoteBasket({ ...remoteBasket }));
    } finally {
      setTokenLoaded(true);
    }
  }, []);

  useEffect(() => {
    getStorageToken();
  }, []);

  if (fontsLoaded && isTokenLoaded) {
    return (
      <Provider store={store}>
        <Navigation />
      </Provider>
    );
  }
  return (
    <View>
      <ActivityIndicator size="large" />
      <AppLoading
        startAsync={getFonts}
        onFinish={() => setLoaded(true)}
        onError={(error) => console.log(error.message)}
        bac
      />
    </View>
  );
}
